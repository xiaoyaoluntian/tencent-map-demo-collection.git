import request from '@/utils/request'
const url = 'https://apis.map.qq.com/ws/geocoder'
export function getLocation(data) {
  console.log(data, 'params')
  return request({
    url: `/qq/ws/geocoder/v1/`, // https://apis.map.qq.com/ws/geocoder/v1/
    method: 'get',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*'
    },
    params: data
  })
}

export function getInfo(token) {
  return request({
    url: '/vue-admin-template/user/info',
    method: 'get',
    params: { token }
  })
}
