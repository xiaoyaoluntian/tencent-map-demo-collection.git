## Vue 集成腾讯地图基础api  Demo集合
### 写作背景 
.之前项目使用腾讯地图，感觉还是比较好用的，但是官方的demo大部分都是原生js，且比较基础，并且很多高级Api分布比较分散，不利于开发者查找，所以使用vue结合网上的开源框架vue-admin模仿官方，做一个开箱即用的Demo集合出来。 down下项目来会有个登录界面，随便输入六个字符就可以了（笔者很懒，懒得移除了，已经没救了）
### 项目预览
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210113162636958.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3hpYW95YW9sdW50aWFu,size_16,color_FFFFFF,t_70#pic_center)
各位看官可以从[这个地址](https://gitee.com/xiaoyaoluntian/tencent-map-demo-collection.git)直接拉取代码 然后复制粘贴就好了
### 项目说明
#### 由于笔者时间仓促，目前只整理了四个模块分别是（如果效果不错将继续更新，欢迎各位道友提issues，看到会及时解决）：

 1. 基础地图引入与展示模块
 2. 3D/2D切换 与效果对比
 3. 关于位置服务的一些基础api 依次为：定位当前位置，定位到初始化位置，定位中心点，添加鼠标点击事件，切换隐藏与显示地图文字
 4. mark标记的基础使用，依次为：添加标记，结束添加标记事件，mark标记点可拖拽。
### 前期准备工作 
#### 点击[这条连接](https://lbs.qq.com?lbs_invite=MV7CFLM)注册腾讯地图开发者账号
### 注意点
这是一个Vue集成腾讯地图的demo
项目中需要在index.html上事先引入以下内容 
   ```html

    <script src="https://map.qq.com/api/gljs?v=1.exp&key=你注册之后获取的key值"></script>
    <script src="https://mapapi.qq.com/web/mapComponents/geoLocation/v/geolocation.min.js"></script>
    <script charset="utf-8" src="https://map.qq.com/api/gljs?libraries=tools&v=1.exp&key=你注册之后获取的key值"></script>
```
然后在main.js 文件下写入这几行代码
```javascript
Vue.prototype.$Map = window.TMap
Vue.prototype.$Location = new window.qq.maps.Geolocation('你自己的key', '腾讯地图模板-博客展示')
```
#### 再次提醒 点击[这条连接](https://lbs.qq.com?lbs_invite=MV7CFLM)可以注册腾讯地图开发者账号。
书到此地，大部分道友应该直接复制粘贴就可以完美的跑起腾讯地图了。

 ####  题外话
 笔者致力于开发遇到的各种复杂组件，虽说授人以鱼不如授人以渔，但是很多情况下我们是需要先恰饭的，所以先把鱼钓上来，希望帮各位刚上路的道友填饱肚子 
#### 以下是我所有组件开源的地址与博客 各位如果觉得有用，别忘了点赞与star，感谢支持

[前端组件集合文章](https://blog.csdn.net/xiaoyaoluntian/article/details/111381970)
[vue组件-echarts地图显示柱状图并添加点击事件文档](https://blog.csdn.net/xiaoyaoluntian/article/details/108920483)
[vue组件之仿钉钉自定义流程图组件文档](https://blog.csdn.net/xiaoyaoluntian/article/details/108871006)
[vue组件之树状选择器组件文档](https://blog.csdn.net/xiaoyaoluntian/article/details/111995326)
最后给我的老东家打个广告，金现代企业股份有限公司做的项目真的不错，在里面带的一年里，成长很多，尤其在那里接触到了UML统一建模语言，对专业技能提升真的很大。在此感恩。

